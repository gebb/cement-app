using HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HeidelbergCement.CaseStudies.Concurrency.Infrastructure.DbContexts.Schedule;

public class ScheduleDbContext: DbContext, IScheduleDbContext
{
    public ScheduleDbContext(DbContextOptions<ScheduleDbContext> options) : base(options)
    {
    }
    public DbSet<Domain.Schedule.Models.Schedule> Schedules { get; set; }
    public DbSet<ScheduleItem> ScheduleItems { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        SetupDateTimeConversion(modelBuilder);

        modelBuilder.Entity<ScheduleItem>().HasKey(it => it.ScheduleItemId);
        modelBuilder.Entity<ScheduleItem>().Property(x => x.ScheduleId).ValueGeneratedNever();
        modelBuilder.Entity<ScheduleItem>().Property(it => it.Start).IsRequired();
        modelBuilder.Entity<ScheduleItem>().Property(it => it.End).IsRequired();
        modelBuilder.Entity<ScheduleItem>().Property(it => it.NumberOfTimesUpdated);
        modelBuilder.Entity<ScheduleItem>().Property(it => it.CementType).IsRequired();
        modelBuilder.Entity<ScheduleItem>().Property(it => it.UpdatedOn).IsRequired();

        modelBuilder.Entity<ScheduleItem>()
            .HasOne(x => x.Schedule)
            .WithMany(bl => bl.ScheduleItems)
            .HasForeignKey(x => x.ScheduleId);

        modelBuilder.Entity<Domain.Schedule.Models.Schedule>().HasKey(it => it.ScheduleId);
        modelBuilder.Entity<Domain.Schedule.Models.Schedule>().Property(it => it.PlantCode).IsRequired();
        modelBuilder.Entity<Domain.Schedule.Models.Schedule>().Property(it => it.UpdatedOn).IsRequired();
    }
    public void SetModified(object entity)
    {
        Entry(entity).State = EntityState.Modified;
    }

    private void SetupDateTimeConversion(ModelBuilder modelBuilder)
    {
        var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
            v => v.ToUniversalTime(),
            v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

        var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
            v => v.HasValue ? v.Value.ToUniversalTime() : v,
            v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);

        foreach (var entityType in modelBuilder.Model.GetEntityTypes())
        {
            if (entityType.IsKeyless)
            {
                continue;
            }

            foreach (var property in entityType.GetProperties())
            {
                if (property.ClrType == typeof(DateTime))
                {
                    property.SetValueConverter(dateTimeConverter);
                }
                else if (property.ClrType == typeof(DateTime?))
                {
                    property.SetValueConverter(nullableDateTimeConverter);
                }
            }
        }
    }

    public async Task<T> DoInTransaction<T>(Func<Task<T>> action, CancellationToken cancellationToken = default)
    {
        // T result = await action();
        // return result;

        // To resurrect the concurrency bug, comment all lines below and uncomment the ones above.

        try
        {
            await using IDbContextTransaction transaction =
                await Database.BeginTransactionAsync(System.Data.IsolationLevel.Serializable, cancellationToken);
            T result = await action();
            await transaction.CommitAsync(cancellationToken);
            return result;
        }
        catch (InvalidOperationException ex) when (
            ex.InnerException is DbUpdateException updEx
            && updEx.InnerException is Npgsql.PostgresException { SqlState : "40001" })
        {
            // 40001: "could not serialize access due to concurrent update"
            // or "could not serialize access due to read/write dependencies among transactions"

            // Throw our custom exception to simplify retry logic (not implemented in this toy project)
            // and error error handling (this exception gets converted to a speceial HTTP status code).
            throw new ConflictException(
                "The attempted operation conflicted with another one performed in parallel.",
                ex);
        }
    }

    public Task DoInTransaction(Func<Task> action, CancellationToken cancellationToken = default)
    {
        return DoInTransaction(action, cancellationToken);
    }

    async Task<int> IDbContext.SaveChangesAsync(CancellationToken cancellationToken)
    {
        // This delay is put in place to ensure that the concurrency problem is reproducible.
        await Task.Delay(1000, cancellationToken);
        return await base.SaveChangesAsync(cancellationToken);
    }
}