using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace HeidelbergCement.CaseStudies.Concurrency.Infrastructure.DbContexts;

public interface IDbContext
{
    int SaveChanges();

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

    void RemoveRange(IEnumerable<object> entities);

    DatabaseFacade Database { get; }

    void Dispose();

    DbSet<T> Set<T>() where T : class;

    void SetModified(object entity);

    public Task<T> DoInTransaction<T>(Func<Task<T>> action, CancellationToken cancellationToken = default);

    public Task DoInTransaction(Func<Task> action, CancellationToken cancellationToken = default);
}