namespace HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Models;

public class Schedule
{
    #pragma warning disable CS8618
    /// <summary>
    /// Used by EF. Keep empty.
    /// </summary>
    protected Schedule()
    {
        // Initializing collection navigation props in the constructor used exclusively by EF has a drawback.
        // If we forget an `Include` when loading this entity from the DB,
        // everything will look like the collection is empty and operations on it are allowed,
        // when in fact we just forgot to load it.
        // It's better to have a NullReferenceException in this case,
        // hopefully occuring in a test and catching the forgotten include.
        // Do not do this: ScheduleItems = new List<ScheduleItem>();
    }
    #pragma warning restore

    public Schedule(int plantCode, DateTime now)
    {
        PlantCode = plantCode;
        UpdatedOn = now;
        ScheduleItems = new List<ScheduleItem>();
    }
    public int ScheduleId { get; private set; }
    public int PlantCode { get; private set; }
    public DateTime UpdatedOn { get; private set; }
    public List<ScheduleItem> ScheduleItems { get; private set; }

    public void AddItem(DateTime start, DateTime end, string cementType, DateTime now)
    {
        var item = new ScheduleItem(this, start, end, cementType, now, ScheduleItems);
        ScheduleItems.Add(item);
    }

    public void UpdateItem(int itemId, DateTime start, DateTime end, string cementType, DateTime now)
    {
        var item = ScheduleItems.First(x => x.ScheduleItemId == itemId);
        item.Update(start, end, cementType, now, ScheduleItems);
        UpdatedOn = now;
    }
}