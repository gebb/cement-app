using System.ComponentModel.DataAnnotations;
using HeidelbergCement.CaseStudies.Concurrency.Common.Validation;

namespace HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Models;

public class ScheduleItem
{
    #pragma warning disable CS8618
    /// <summary>
    /// Used by EF. Keep empty.
    /// </summary>
    protected ScheduleItem()
    {
    }
    #pragma warning restore

    public ScheduleItem(
        Schedule parent,
        DateTime start,
        DateTime end,
        string cementType,
        DateTime updatedOn,
        List<ScheduleItem> scheduleItems)
    {
        DateValidator.ValidateRange(start, end);
        ValidateDoesNotOverlapWithItems(start, end, scheduleItems);

        Schedule = parent;
        Start = start;
        End = end;
        CementType = cementType;
        UpdatedOn = updatedOn;
        NumberOfTimesUpdated = 0;
    }
    public int ScheduleItemId { get; set; }
    public DateTime Start { get; private set; }
    public DateTime End { get; private set; }
    public string CementType { get; private set; }
    public DateTime UpdatedOn { get; private set; }
    public int NumberOfTimesUpdated { get; private set; }
    public Schedule Schedule { get; private set; }
    public int ScheduleId { get; private set; }

    public void ValidateDoesNotOverlapWithItems(
        DateTime start,
        DateTime end,
        List<ScheduleItem> scheduleItems)
    {
        ScheduleItem? conflict = scheduleItems.FirstOrDefault(scheduleItem =>
            this != scheduleItem &&
            start < scheduleItem.End && scheduleItem.Start < end);
        if (conflict != null)
        {
            throw new ValidationException(
                $"There is a conflict with the other planned item (Id={conflict.ScheduleItemId}).");
        }
    }

    public void Update(
        DateTime start,
        DateTime end,
        string cementType,
        DateTime now,
        List<ScheduleItem> scheduleItems)
    {
        DateValidator.ValidateRange(start, end);
        ValidateDoesNotOverlapWithItems(start, end, scheduleItems);

        Start = start;
        End = end;
        CementType = cementType;
        UpdatedOn = now;
        NumberOfTimesUpdated++;
    }
}