# Short summary of DDD

DDD stands for Domain Driven Design. As I see it, it's a set of patterns and principles of
developing complex software, aimed at optimizing maintainability and minimizing complexity.

## Principles

- Develop the so-called "ubiquitous language" which developers, domain experts and stakeholders speak.
  **Motivation**: This minimizes the mental effort of translating business terms to software
  terms and vice versa.
- Focus on your domain, make its implementations as clear as and decluttered as possible.
  Apply all the principles of good OOP and best practices in the Domain module.
  **Motivation**: The business domain is often complex enough on its own, so that
  mixing in other concerns (infrastructure) leads to developers
  being overwhelmed with complexity, and hence to bugs and slowing down of writing new
  features.

## Concepts and Patterns

I will just list some concepts that Eric Evans describes in his book that I find
most useful.

- Bounded context (in the modern world this roughly corresponds to a microservice)
- Anticorruption layer (protects the domain model from foreign concepts leaking in)
- Application service (combines domain logic and infrastructure logic)
- Aggregate (consistency boundary)
- Aggregate root (the single entity through which the aggregate is mutated)
- Entity (an object with its own lifetime and identity)
- Value object (an immutable object defined by only values of its properties)
- Shared kernel (a subset of domain model shared by many bounded contexts)

## Alternatives to DDD

DDD is maybe not the best choice for small, hacky, experimental projects
where properly designing a rich domain model is not warranted.
One should consider the transaction script and the anemic domain model (anti-)patterns.

## Drawbacks in the current solution

I will describe the drawbacks of the choice I made: transactions vs. custom optimistic
concurrency.

- The logic to retry on transient exceptions like transaction conflicts
  must be implemented by the developer (vs. using a built-in EF retry strategy)
  and added where it makes sense. In my experience it makes sense in most cases to wrap the
  entire transaction in a retry routine, but I did not implement it in this toy project.
- Postgres may be too careful when detecting conflicts and abort a
  concurrent transaction even if in reality there's no conflict.
  In my experience, retries help with that.

### Pre-existing drawbacks

- Overdesign.
  - App services don't need interfaces.
    - They would be useful if one decided to write tests of controllers and mock their
      dependencies, which one should not:
      controllers have no logic. Or if one decided to inject one app service to another,
      which one also should not, because the wrapper service doesn't know whether
      the injected service performs a transaction, so an HTTP-request may end up executing
      many DB-transactions instead of one, losing atomicity (imagine the second transaction
      fails).
    - Interfaces annoyingly break the "go to definition" feature.
      You press F12 on a called method and end up in the interface instead of where you
      really want: the implementation.
    - Interfaces are extra code to read and maintain.
  - Custom repositories.
    - Again, extra code without benefits.
      EF implements the Repository pattern (as well as the Unit of Work pattern)
      adequately enough.
      You may think that you need these abstractions to mock these dependencies
      in fast-running tests of app services, but mocking can be replaced by tests with
      real objects (DbContext) and an in-memory DB (e.g. sqlite) with approximately the
      same performance, but much cleaner and closer to real life.
- Repository interface in the Domain module.
  This one is a bit controversial, because Eric Evans himself puts it there in his examples.
  I disagree with him on this point. A repository is an infrastructure concept, and may
  easily pollute domain APIs with things like `Task`s, which seems like a trifle, but it's
  a step away from purity and simplicity. I maintain that the Domain module
  should be completely free from infrastructure concerns. The price for that is app services
  get a bit more complicated, but not critically so, in my opinion.
- Nomenclature (a small one). What's called here "Models" (see this namespace in the Domain
  module) is called "Entities" in DDD. Probably the confusion with the model from
  MV*-patterns.
- Bugs in Domain. These were not mentioned in the assignment descriptions, have nothing to do with concurrency,
  and are reproducible in a fully sequential access scenario, as demonstrated by
  [property-based tests](./HeidelbergCement.CaseStudies.Concurrency.Domain.PropTests) that I wrote.
  ![tests found bugs](./img/bugs_in_domain.png)