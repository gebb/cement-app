using HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Models;
using HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Repositories;
using HeidelbergCement.CaseStudies.Concurrency.Dto.Input;
using HeidelbergCement.CaseStudies.Concurrency.Dto.Response;
using HeidelbergCement.CaseStudies.Concurrency.Extensions;
using HeidelbergCement.CaseStudies.Concurrency.Infrastructure.DbContexts.Schedule;
using HeidelbergCement.CaseStudies.Concurrency.Services.Interfaces;

namespace HeidelbergCement.CaseStudies.Concurrency.Services;

public class SchedulesService: ServiceBase<IScheduleDbContext>, IScheduleService
{
    private readonly DateTime _now = DateTime.UtcNow;
    private readonly IScheduleRepository _scheduleRepository;
    public SchedulesService(IScheduleDbContext dbContext, IScheduleRepository scheduleRepository) : base(dbContext)
    {
        _scheduleRepository = scheduleRepository;
    }

    public async Task<ScheduleResponseDto> GetLatestScheduleForPlant(int plantCode)
    {
        var currentDraftSchedule = await _scheduleRepository.GetLastUpdatedScheduleForPlant(plantCode);
        if (currentDraftSchedule == null)
        {
            throw new Exception($"There is no draft schedule for plant {plantCode}");
        }

        return currentDraftSchedule.MapToScheduleDto();
    }

    public async Task<ScheduleResponseDto> AddItemToSchedule(int scheduleId, ScheduleInputItemDto scheduleItem)
    {
        return await DbContext.DoInTransaction(async () => {
            var scheduleWithId = await _scheduleRepository.GetScheduleById(scheduleId);
            scheduleWithId.AddItem(
                start: scheduleItem.Start,
                end: scheduleItem.End,
                cementType: scheduleItem.CementType,
                now: _now);
            await DbContext.SaveChangesAsync();
            return scheduleWithId.MapToScheduleDto();
        });
    }

    public async Task<ScheduleResponseDto> AddNewSchedule(int plantCode, List<ScheduleInputItemDto> scheduleInputItems)
    {
        return await DbContext.DoInTransaction(async () => {
            var schedule = new Schedule(plantCode, _now);
            if (scheduleInputItems != null)
            {
                // PERF: This operation is O(N^2) where N = scheduleInputItems.Count,
                // because AddItem is O(N).
                // Optimize if the items collection is planned to be big.
                foreach (var scheduleInputScheduleItem in scheduleInputItems)
                {
                    schedule.AddItem(
                        start: scheduleInputScheduleItem.Start,
                        end: scheduleInputScheduleItem.End,
                        cementType: scheduleInputScheduleItem.CementType,
                        now: _now);
                }
            }

            DbContext.Schedules.Add(schedule);
            await DbContext.SaveChangesAsync();
            return schedule.MapToScheduleDto();
        });
    }

    public async Task<ScheduleResponseDto> ChangeScheduleItem(
        int scheduleId,
        int itemId,
        ScheduleInputItemDto dto)
    {
        return await DbContext.DoInTransaction(async () => {
            var schedule = await _scheduleRepository.GetScheduleById(scheduleId);
            schedule.UpdateItem(itemId, dto.Start, dto.End, dto.CementType, _now);
            await DbContext.SaveChangesAsync();
            return schedule.MapToScheduleDto();
        });
    }
}