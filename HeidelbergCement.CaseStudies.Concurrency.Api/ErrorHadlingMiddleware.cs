using System.Diagnostics;
using System.Runtime.ExceptionServices;
using HeidelbergCement.CaseStudies.Concurrency.Infrastructure;

namespace HeidelbergCement.CaseStudies.Concurrency;


/// <summary>
/// A middleware for handling exceptions in the application. Produces a proper response for an exception.
/// Unlike the standard middleware, logs selected exceptions as warnings.
/// </summary>
public class ErrorHandlerMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger _logger;
    private readonly Func<object, Task> _clearCacheHeadersDelegate;

    private static readonly Action<ILogger, Exception> _logUnhandledException = LoggerMessage.Define(
        LogLevel.Error,
        new EventId(1, "UnhandledException"),
        "An unhandled exception has occurred while executing the request.");

    private static readonly Action<ILogger, Exception> _logResponseStarted = LoggerMessage.Define(
        LogLevel.Warning,
        new EventId(2, "ResponseStarted"),
        "The response has already started, the error handler will not be executed.");

    /// <summary>
    /// Creates a new <see cref="ErrorHandlerMiddleware"/>
    /// </summary>
    /// <param name="next">The <see cref="RequestDelegate"/> representing the next middleware in the pipeline.</param>
    /// <param name="loggerFactory">The <see cref="ILoggerFactory"/> used for logging.</param>
    public ErrorHandlerMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
    {
        _next = next;
        _logger = loggerFactory.CreateLogger<ErrorHandlerMiddleware>();
        _clearCacheHeadersDelegate = ClearCacheHeaders;
    }

    /// <summary>
    /// Executes the middleware.
    /// </summary>
    /// <param name="context">The <see cref="HttpContext"/> for the current request.</param>
    public Task Invoke(HttpContext context)
    {
        ExceptionDispatchInfo edi;
        try
        {
            Task task = _next(context);
            if (!task.IsCompletedSuccessfully)
            {
                return Awaited(this, context, task);
            }

            return Task.CompletedTask;
        }
        catch (Exception exception)
        {
            // Get the Exception, but don't continue processing in the catch block as its bad for stack usage.
            edi = ExceptionDispatchInfo.Capture(exception);
        }

        return HandleException(context, edi);

        static async Task Awaited(ErrorHandlerMiddleware middleware, HttpContext context, Task task)
        {
            ExceptionDispatchInfo? edi = null;
            try
            {
                await task;
            }
            catch (Exception exception)
            {
                // Get the Exception, but don't continue processing in the catch block as its bad for stack usage.
                edi = ExceptionDispatchInfo.Capture(exception);
            }

            if (edi != null)
            {
                await middleware.HandleException(context, edi);
            }
        }
    }

    private async Task HandleException(HttpContext context, ExceptionDispatchInfo edi)
    {
        Exception error = edi.SourceException;
        if (error is ConflictException wae)
        {
            LogConflictException(wae);
        }
        else
        {
            _logUnhandledException(_logger, error);
        }

        // We can't do anything if the response has already started, just abort.
        if (context.Response.HasStarted)
        {
            _logResponseStarted(_logger, null!);
            edi.Throw();
        }

        PathString originalPath = context.Request.Path;
        context.Response.OnStarting(_clearCacheHeadersDelegate, context.Response);
        var extensions = new Dictionary<string, object?>
        { { "traceId", Activity.Current?.Id ?? context.TraceIdentifier } };

        // JSON Problem Details.
        IResult result = error switch
        {
            ConflictException ex => Results.Problem(
                detail: ex.Message,
                title: "Conflict",
                statusCode: StatusCodes.Status409Conflict,
                extensions: extensions),
            BadHttpRequestException ex => Results.Problem(
                detail: ex.Message,
                statusCode: ex.StatusCode,
                extensions: extensions),
            _ => Results.Problem(statusCode: StatusCodes.Status500InternalServerError, extensions: extensions)
        };
        await result.ExecuteAsync(context);
    }

    private void LogConflictException(ConflictException ex)
    {
        _logger.LogWarning("{ErrorType}: {ErrorMessage}", nameof(ConflictException), ex.Message);
    }

    private static Task ClearCacheHeaders(object state)
    {
        IHeaderDictionary? headers = ((HttpResponse)state).Headers;
        headers.CacheControl = "no-cache,no-store";
        headers.Pragma = "no-cache";
        headers.Expires = "-1";
        headers.ETag = default;
        return Task.CompletedTask;
    }
}