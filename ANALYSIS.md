# The problem

The problem was caused by a concurrency issue leading to a broken invariant of
the Schedule aggregate, which is "intervals of items don't overlap".

The events unfolded as follows.

1. Request 1 reads the state of the Schedule aggregate from the DB.
2. Request 2 reads the same state.
3. Request 1 creates a ScheduleItem and updates the DB.
4. Request 2, not "knowing" about the parallel mutation, creates a ScheduleItem overlapping
   with the one from step 3 (or updates one with the same effect).

The resulting state of the Schedule aggregate in the DB is inconsistent: some ScheduleItems overlap.

# Possible solutions

There are definitely more possible solutions, but I will describe only two.
1. Implement optimistic concurrency as described in the [EF docs](https://docs.microsoft.com/en-us/ef/core/saving/concurrency):

   - Add a special property to the aggregate root (Schedule) EF calls this property
     a "concurrency token"). This property acts as a version of the entire aggregate.
   - Configure EF to use this property to detect conflicts.
2. Use transactions and let Postgres detect conflicts.
   Specifically, wrap the body of each app service method in a transaction.
   Postgres uses [MVCC](https://en.wikipedia.org/wiki/Multiversion_concurrency_control) under the hood,
   so it's still optimistic concurrency.

# Implemented solution

I chose option 3 because of these advantages:

- When the domain model gets complex, I don't need to think about whether my home-grown
  versioning mechanism prevents all possible race conditions. The isolation built into
  Postgres gives me the strongest possible guarantee: the state of the DB will always look
  like transactions were performed sequentially, each transaction "seeing" the effects of
  all previous ones. See [Postgres docs](https://www.postgresql.org/docs/current/transaction-iso.html#XACT-SERIALIZABLE).
- I don't need to remember to increment the aggregate version in all methods mutating
  the aggregate. From experience I know this happens and it's hard to catch.
