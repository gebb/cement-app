module HeidelbergCement.CaseStudies.Concurrency.Domain.PropTests
open System
open Expecto
open Hedgehog
open Tools
open HeidelbergCement.CaseStudies.Concurrency.Domain.Schedule.Models
open System.Globalization
open System.Reflection

// Format dates in test output.
let customCulture = CultureInfo.InvariantCulture.Clone() :?> CultureInfo
let dtPattern =
  typeof<DateTimeFormatInfo>
    .GetField("generalLongTimePattern", BindingFlags.NonPublic ||| BindingFlags.Instance);
dtPattern.SetValue(customCulture.DateTimeFormat, "yyyy-MM-dd")
let offsetPattern =
  typeof<DateTimeFormatInfo>
    .GetField("dateTimeOffsetPattern", BindingFlags.NonPublic ||| BindingFlags.Instance);
offsetPattern.SetValue(customCulture.DateTimeFormat, "yyyy-MM-dd")
CultureInfo.CurrentCulture <- customCulture

/// A "blueprint" for a future `Command`.
type CommandKind = CmAddItem | CmUpdateItem

type AddItemArgs = {ItemId: int; Start: DateTime; End: DateTime}
type UpdateItemArgs = {ItemId: int; Start: DateTime; End: DateTime}

/// A command with arguments that is planned to be executed on the aggregate.
type Command =
  | AddItem of AddItemArgs
  | UpdateItem of UpdateItemArgs


module GenModel =
  /// An immutable structure that holds the state of created entities during command generation
  /// to prevent generationg illegal commands (like updating an entity with a non-existing Id).
  type Db = {
    ScheduleItems: Set<int> // set of ids
  }
  with
    static member Empty = {
      ScheduleItems = Set.empty
    }

  /// Gets the id for a new entity in the set.
  let private nextId (s: Set<int>) =
    if Set.isEmpty s then 1 else (Set.maxElement s) + 1

  /// Adds a new ScheduleItem to the DB.
  /// Returns a new DB with the added entity and the entity's Id.
  let addScheduleItem (db: Db): Db * int =
    let newId = nextId db.ScheduleItems
    {db with ScheduleItems = db.ScheduleItems |> Set.add newId }, newId

/// Generates random objects.
module Gen =
  open Microsoft.FSharp.Reflection
  open GenModel

  /// Construcs a discriminated union value based on its reflection info.
  let private construct<'T> (caseInfo: UnionCaseInfo) =
    FSharpValue.MakeUnion(caseInfo, [||]) :?> 'T

  /// Generates a random `CommandKind` value.
  let private genCommandKind: Gen<CommandKind> =
    let cases: CommandKind[] = FSharpType.GetUnionCases typeof<CommandKind> |> Array.map construct
    Gen.item cases

  /// Generates a random date interval (possibly invalid, i.e. end > start).
  let private genPeriod: Gen<DateTime * DateTime> = gen {
    let! offset = Range.exponential 1 300 |> Gen.int32
    let periodStart = (DateTime (2000, 1, 1)).AddDays(offset)
    let! periodLength = Range.linear 1 5 |> Gen.int32
    let periodEnd = periodStart.AddDays(periodLength)
    let! isInvalid = Gen.bool
    return if isInvalid then (periodEnd, periodStart) else (periodStart, periodEnd)
  }

  /// Generates a random AddItem command.
  let private genAddItem (db: Db): Gen<Command * Db> = gen {
    let! itemStart, itemEnd = genPeriod
    let newDb, newId = addScheduleItem db
    let command = AddItem {ItemId = newId; AddItemArgs.Start = itemStart; End = itemEnd }
    return (command, newDb)
  }

  /// Generates a random UpdateItem command.
  let private genUpdateItem (db: Db): Gen<Command * Db> = gen {
    let! itemId = Gen.item db.ScheduleItems
    let! itemStart, itemEnd = genPeriod
    let command = UpdateItem {ItemId = itemId; Start = itemStart; End = itemEnd}
    return (command, db)
  }

  // Generates a sequence of random commands to be executed on a `Schedule` object.
  let genCommands : Gen<Command[]> = gen {
    let! cms = Gen.list (Range.exponential 0 50) genCommandKind
    let nextCommand (soFar: List<Command>, db: Db) (c: CommandKind): Gen<list<Command> * Db> = gen {
      let! next =
        match c with
        | CmAddItem -> gen {
            let! addItem, newDb = genAddItem db
            return (addItem :: soFar, newDb)
          }
        | CmUpdateItem -> gen {
            if Set.isEmpty db.ScheduleItems then
              // Must add an item before trying to update.
              let! addItem, db1 = genAddItem db
              let! updateItem, db2 = genUpdateItem db1
              return (updateItem :: addItem :: soFar, db2)
            else
              let! updateItem, newDb = genUpdateItem db
              return (updateItem :: soFar, newDb)
          }
      return next
    }
    let! comms, _ = Gen.foldM nextCommand ([], Db.Empty) cms
    return comms |> List.rev |> List.toArray
  }

/// Represents the result of applying a sequence of commads to a `Schedule`.
type ExecutionResult = PreconditionFalied | Success

/// Executes the specified list of commands on the specified `Schedule` aggregate.
let executeCommands (schedule: Schedule) (cmds : seq<Command>): ExecutionResult =
  let now = DateTime (2000, 1, 1)
  let cementType = "type-1"
  try
    for c in cmds do
      match c with
      | AddItem {ItemId = itemId; Start = itemStart; End = itemEnd} ->
        schedule.AddItem(itemStart, itemEnd, cementType, now)
        let x = schedule.ScheduleItems |> Seq.last
        x.ScheduleItemId <- itemId
      | UpdateItem {ItemId = itemId; Start = itemStart; End = itemEnd} ->
        schedule.UpdateItem(itemId, itemStart, itemEnd, cementType, now)
    Success
  with
  | :? ComponentModel.DataAnnotations.ValidationException -> PreconditionFalied

/// Checks that any two items of the specified `Schedule` overlap.
/// Returns the first such pair of items.
let tryGetOverlapping (s: Schedule) : Option<{| Current: (DateTime*DateTime); Next: (DateTime*DateTime)|}> =
  s.ScheduleItems
  |> Seq.sortBy (fun i -> i.Start)
  |> Seq.pairwise
  |> Seq.tryFind (fun (i1, i2) -> i1.End > i2.Start)
  |> Option.map (fun (i1, i2) -> {| Current = (i1.Start, i1.End); Next = (i2.Start, i2.End) |})

/// Checks if there is an item with Start >= End.
/// Returns the period of the first such item.
let tryGetInvalidPeriod (s: Schedule) : Option<(DateTime*DateTime)> =
  s.ScheduleItems
  |> Seq.tryFind (fun x -> x.Start >= x.End)
  |> Option.map (fun x -> x.Start, x.End)

// Hdgehog test config. The default is 100 tests, but since these are fast we can afford more.
let conf =
  PropertyConfig.defaultConfig
  |> PropertyConfig.withTests 600<tests>

[<Tests>]
let initProcedureTests = testList "Shedule" [
  testCase "Items don't overlap" <| fun () ->
    property {
      let! cmds = Gen.genCommands
      let schedule = Schedule (1, DateTime.UtcNow)
      match executeCommands schedule cmds with
      | PreconditionFalied -> return true
      | Success ->
        let overlapping = tryGetOverlapping schedule
        return! invariant [
          <@ overlapping |> Option.isNone @>
        ]
    } |> Property.checkWith conf

  testCase "For each item start < end" <| fun () ->
    property {
      let! cmds = Gen.genCommands
      let schedule = Schedule (1, DateTime.UtcNow)
      match executeCommands schedule cmds with
      | PreconditionFalied -> return true
      | Success ->
        let invalid = tryGetInvalidPeriod schedule
        return! invariant [
          <@ invalid |> Option.isNone @>
        ]
    } |> Property.checkWith conf
]
